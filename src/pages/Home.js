import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home() {
  const data = {
    title: "iVape",
    content: "Level up your vaping experience with, iVape",
    destination: "/products",
    label: "Buy now!",
  };

  return (
    <>
      <Banner data={data} />
      <Highlights />
    </>
  );
}
