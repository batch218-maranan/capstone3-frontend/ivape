import { useState, useEffect, useContext } from "react";
import { Form, Button, Container } from "react-bootstrap";
import { NavLink, Navigate } from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import vape from "../components/img/vape.jpg";

import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(true);

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (typeof data.access !== "undefined") {
          localStorage.setItem("token", data.access);

          retrieveUserDetails(data.access);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to iVape!",
          });
        } else {
          Swal.fire({
            title: "Authentication Failed",
            icon: "error",
            text: "Please, check your login details and try again.",
          });
        }
      });

    setEmail("");
    setPassword("");
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (

    <Container fluid>
      <Row>
        <Col>
          <Image fluid src={vape} />
        </Col>
        <Col className="my-auto">
          <div className="text-center fs-1 fw-bold m-4">iVape</div>
          <div className="text-uppercase text-center fs-2 fw-semibold m-4">
            login to your account
          </div>
          <Form onSubmit={(e) => authenticate(e)} className="fw-semibold">
            <Form.Group controlId="userEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter your email address"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter your password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
              <Form.Check
                type="checkbox"
                label="Remember me"
                className="mt-3"
              />
              <Form.Text>
                Don't have an account? Register <NavLink to="/register">here</NavLink>
              </Form.Text>
            </Form.Group>

            {isActive ? (
              <Button
                variant="success"
                type="submit"
                id="submitBtn"
                className="mt-4"
              >
                Login
              </Button>
            ) : (
              <Button
                variant="success"
                type="submit"
                id="submitBtn"
                disabled
                className="mt-4"
              >
                Login
              </Button>
            )}
          </Form>
        </Col>
      </Row>
    </Container>

  );
}
