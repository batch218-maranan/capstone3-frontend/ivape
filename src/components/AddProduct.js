import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Swal from "sweetalert2";

export default function AddProduct(productId) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [name, setName] = useState(""); 
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  function addProduct(productId, option) {
    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price
        
      })
    })
      .then((res) => res.json())
      .then((data) => {
        Swal.fire({
            title: "Successfully added",
            icon: "success",
            text: "You have successfully added this product.",
          });
          setName("")
          setDescription("")
          setPrice("")
          handleClose()
      });
  }


  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Add New Product
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Product?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicName">
              <Form.Label>Name:</Form.Label>
              <Form.Control type="text" value={name} onChange={e => {
                setName(e.target.value)
              }} />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicDescription">
              <Form.Label>Description:</Form.Label>
              <Form.Control type="text" value={description} onChange={e => {
                setDescription(e.target.value)
              }} />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPrice">
              <Form.Label>Price:</Form.Label>
              <Form.Control type="number" value={price} onChange={e => {
                if(e.target.value <= 0){
                    return
                }
                setPrice(e.target.value)
              }} />
            </Form.Group>

            {/* <Button variant="primary" type="submit">
              Submit
            </Button> */}
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={addProduct}>
            Add Product
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
